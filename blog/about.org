#+SETUPFILE: ../templates/org/common-header.org
#+TITLE: About
#+DATE: <2020-04-05 Sun>
#+KEYWORDS:about

I am a Computer Researcher by profession, an Emacs enthusiast, an armature poet/writer by hobby and have a new found interest towards Human Behaviour. You can find me on [[https://scholar.google.com/citations?user=AND3-z8AAAAJ][Google Scholar]] or [[https://www.researchgate.net/profile/Justin-Jose-12][Research Gate]] for all the research work, and unearthed my poetic musings at [[https://thetwistedcorridors.blogspot.com/][The Twisted Corridors]]. 

- Author: Justin Jose
- Source Code: [[https://gitlab.com/t3pleni9][GitLab]]
- Twitter: [[https://twitter.com/translucentInk][@translucentInk]]

