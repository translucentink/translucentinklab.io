#+SETUPFILE: ../../../templates/org/common-header.org
#+TITLE: The Selfish *Empathy*
#+DATE: <2020-11-20 Fri>
#+KEYWORDS: soft-skills, planning, empathy
* Empathy - Evil in guise of good?
Empathy unlike its miss-understood sibling, Sympathy, is a newer and more twentieth century word in the English language. Two psychologists were struggling to translate a German word /Einfühlung/ to English, and stuck with what every other researcher would do - fall back to the roots. Empathy literally translates to =in-feeling= and has its roots derived from =em= which means =in= and =pathos= meaning feeling. The dictionary definition also does justice to the translation - A capacity to understand or feel what another person is experiencing from *within* their frame of reference. 
Empathy as a behavior is not new, and has been exhibited in organisms other than humans too. Research has established the presence of empathy in other social organisms, and is seen as an important characteristic for social developments. 
Unlike popular beliefs, empathy need not be acted upon, it just is an ability to feel or understand another persons feeling or predicament. Empathy has picked up quite well in the recent times. Every managerial meeting and every corporate retreat does talk about empathy being the next generation tool, which would help them design products with the end user in mind. Even [[https://arxiv.org/abs/1906.06436][AI]] has not been spared from this. 

One of the recent addition to the bucket of empathy has been the 'Empathetic Planning', which helps in getting the job done by thinking about what others would want, and in this small article, the same thought would be looked at from a different perspective. Can Empathy be used for selfish motives. Selfish empathy does feel a tad bit revolting, but is it really a wolf in a goat's skin?
* The Strategist 
Before delving deeper into the topic of selfish empathy, lets touch upon a different set of people - The Strategists. They come in many forms, and flavours. A kid trying to strategically hide the report card, to an army colonel planning the best attack strategy, everyone uses or misuses empathy. For those who find the term empathy being wrongly placed in the previous sentence, we can look at how a strategist would think. The kid can empathise (feel, going by the literal meaning), what the parent would feel if the result not upto mark, hence the most obvious choice is to hide, but since, the child can think from the shoes of the parent, the child can easily come up with a plan, where the report card can be hidden without it being easily spotted. The kid here is really naive though, as the kid has not yet thought about the scenario from their teacher's perspective, who would most probably send in a note asking to meet the parents, but still, a short term strategy. A similar thought experiment can be tried out with the army colonel planning the best attack strategy. 

Many would argue that the term empathy has been wrongly used in the strategist thought experiment. A counter argument to support this thought would hinge strongly on the fact that in these strategies there is a variable - the Human Behaviour, which when acted upon to derive a suitable path, requires empathy to be exercised. The strategist needs to feel what the other human in their plan feels, in-order to see all the possible paths. With this argument we can now try to define 'Selfish Empathy'.
* The Selfish Empathy
From the previous section we have arrived at a conclusion that a strategist who plans around a variable behavior based environment does require a high sense of empathy in order to make an optimal plan. So can empathy be used for selfish motives by clubbing it with forethought and situational intuition? Lets take it up with a hypothetical scenario:

#+begin_quote
Alice is a perfectionist, and would really get bothered by shabby work. Bob on the other hand doesn't really care about the quality. Alice and Bob are paired together, and Bob is still not so much in a mood to work. Bob however, can think from Alice's perspective and understands how Alice would feel if the work is not done properly, and hence hatches a plan to actually do a shoddy work. Alice being a perfectionist and knowing that the work would not be perfect without Bob doing Bob's part of work perfectly, ends up completing the tasks assigned to Bob too. The work is done, and Bob didn't have much work either.
#+end_quote

In the above scenario, Bob does exhibit a certain level of empathy. Bob places themselves in the shoes of Alice, and thinks about what Alice would feel if the work is not done properly. Bob also does feel it, but Bob uses this knowledge to do exactly the opposite. Bob uses empathy for personal gain. 

So to conclude, empathy really is just a tool, and the person who has this tool does stand to gain a lot, in both the good and popularly accepted sense, and also in a nefarious sense.

* Want to leave a comment?
- Discuss the post on [[https://www.reddit.com/user/translucentInk/comments/jxakb9/the_selfish_empathy/][Reddit]]
- Drop a feedback or comment on [[https://twitter.com/translucentInk/status/1329521518435450883][Twitter]].

