#+SETUPFILE: ../../../templates/org/common-header.org
#+TITLE: Thoughts on *Note taking*
#+DATE: <2023-03-27 Mon>
#+KEYWORDS: noteTaking

* Properties of a good knowledge management system

1. *Atomic* 
The piece of knowledge should be self sustained, and should be comprehensible by itself. It may require pre- reads or other accompanying notes to be sensible in a bigger picture, but individually it should be self sufficient in explaining or capturing the thought or knowledge it represents.
2. *Searchable*
Any note should be independently searchable. This brings in the idea of indexing. The knowledge node should be individually searchable, or should be searchable using a parent tag.  Simple grep command on the root folder is a powerful searching method. Others include =ag=, =deft= etc.
3. *Linked*
A knowledge independently doesn't add value in generating new knowledge, but in combination with other atomic nodes help in generating novel thoughts and ideas. Hence every node in the knowledge should be linked to form a bigger tree, or graph of interconnected thoughts and ideas 


* Who should manage knowledge
- You're a writer, author, or person who wants to create genius-level work in your field—the type of work that will last for over two hundred years.

- You wish to use a system that develops the two most essential skills you'll need for thriving in the future: (1) the skill of getting to know your mind (self-awareness) and (2) the skill of developing your mind's flexibility.

* Thoughts on note taking
#+begin_src quote
It's been put forth by scholars that "reading without noting" yields "superficial knowledge." The reason why is because reading, as an act alone, is "not accompanied by the attention and thought required to make a well-considered note.
#+end_src
- Yeo, Richard. Notebooks, Recollection, and External Memory: Some Early Modern English Ideas and Practices. (Brill, 2016), 138.
- Cevolini, Alberto, ed. Forgetting Machines: Knowledge Management Evolution in Early Modern Europe. Library of the Written Word, volume 53. Leiden ; (Boston: Brill, 2016), 26.

The act of note taking forces a reader to think about the context and content of the read piece and subsequently the note.

** How to make effective notes
There are four key steps to the Feynman Technique:

1. Choose a concept you want to learn about
2. Explain it to a 12 year old
3. Identify knowledge gap: Reflect, Refine, and Simplify
4. Organize and Review
#+begin_src quotes
The person who says he knows what he thinks but cannot express it usually does not know what he thinks.

- Mortimer Adler
#+end_src
* Knowledge Management Frameworks
- [[https://en.wikipedia.org/wiki/Zettelkasten][Zettelkasten]]
- [[https://workflowy.com/systems/para-method/][P.A.R.A]]: Project, Areas, Resources, Archives. 
The Key Principle – Organizing Information By Your Projects And Goals
  - Summarize only when needed:  [[https://fortelabs.com/blog/progressive-summarization-a-practical-technique-for-designing-discoverable-notes/?ref=zain-rizvi][Progressive Summarization]]
  - Zettelkasten is a note-taking method designed to spark new insights. PARA is not. It's a way of organizing files and information for a specific project. The Zettelkasten focuses on how to think and come up with associations.
- [[https://www.reddit.com/r/Zettelkasten/comments/10tpngb/concepts_maps_and_zettelkasten/?utm_source=share&utm_medium=android_app&utm_name=androidcss&utm_term=1&utm_content=share_button][Concept maps]]
- Wallis, Steven. (2015). The Science of Conceptual Systems: A Progress Report. Foundations of Science. In Press. 10.1007/s10699-015-9425-z.  [[https://www.researchgate.net/publication/282247735_The_Science_of_Conceptual_Systems_A_Progress_Report][The Science of Conceptual Systems: A Progress Report]]

* Argument for handwritten notes
#+begin_src quotes
Unlike digital notes, which may grow endlessly due to virtually unlimited space limits, notes in an Antinet serve as a cue for generating the recall process in your mind. Digital notes tend to take a different form; they tend to be all-encompassing and thorough, which gets overwhelming. Your notes should be a communication experience that takes place when you use them to write your book, essay, blog post, or paper.
#+end_src

1. Digital notes can grow endlessly, instead of handwritten which forces you to be as precise and concise as possible due to the limited space of the physical writting medium.
2. The act of writing creates a mental node about the note itself, the color, the links and the tags.

#+begin_src quotes
Handwriting forces your brain to mentally engage with the information, improving both literacy and reading comprehension. On the other hand, typing encourages verbatim notes without giving much thought to the information.
#+end_src
* Change the system, not yourself
#+begin_src quotes
Change myself to match the system? I'll run out of steam trying to live my life on pure grit and will power. Instead, why not change the system to match me?
#+end_src
[[https://www.zainrizvi.io/blog/para-vs-zettelkasten-the-false-binary/][P.A.R.A vs Zettelkasten]]

