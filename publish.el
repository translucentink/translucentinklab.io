;; publish.el --- Publish org-mode title on Gitlab Pages
;; Author: Justin

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.

;;; Code:

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-plus-contrib)
(package-install 'htmlize)

(require 'cl-lib)
(require 'org)
(require 'ox-publish)
(require 'ox-rss)

;; setting to nil, avoids "Author: x" at the bottom

(defvar ti_title "Translucent *Ink*")
(defvar ti_root "https://translucentink.gitlab.io/")

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")

(defun my/all-format-entry (entry style project)
  "Return a good display structure"
  
  (cond ((not (directory-name-p entry))
         (let* ((title (org-publish-find-title entry project))
                (date (format-time-string "%d %b, %Y" (org-publish-find-date entry project))))
           (with-temp-buffer
             (insert (format "~%s~ [[file:%s][%s]]\n" date entry title))
             (buffer-string))))
        ((eq style 'tree)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

(defun my/format-entry (entry style project)
  "Return a good display structure"
  
  (cond ((not (directory-name-p entry))
         (let* ((title (org-publish-find-title entry project))
                (date (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))))
           (with-temp-buffer
             (insert (format "~%s~ [[file:%s][%s]]\n" date entry title))
             (buffer-string))))
        (t "SKIP-ENTRY")))

(defun my/all-sitemap-function (title &optional posts)
  (concat
   "#+SETUPFILE: ../../templates/org/sitemap-header.org\n\n" 
   "#+TITLE: " title "\n\n"
	(org-list-to-org posts) "\n\n"
	))

(defun my/blog-sitemap-function (title &optional posts)
  (let* ((content (with-temp-buffer
                    (insert (org-list-to-org posts))
                    (beginning-of-buffer)
                    (flush-lines "SKIP-ENTRY")
                    (goto-line 11)
                    (flush-lines "")
                    (buffer-string))))
  (concat
   "#+SETUPFILE: ../../templates/org/sitemap-header.org\n\n" 
   "#+TITLE: " title "\n\n"
   "- Posts \n\n"
   content "\n\n"
	)))

(defvar current-date-time-format "%d %b %Y, %H:%M:%S %Z"
  "Format of date to insert with `insert-current-date-time' func
See help of `format-time-string' for possible replacements")


(defun my/html-postamble ()
    (with-temp-buffer
      (insert-file-contents "./templates/html/licence.html")
      (concat "<hr>"
	      (buffer-string))))

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg" "png"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "./blog/posts"
             :base-extension "org"
             :recursive t
	           :sitemap-title ti_title
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft" "templates" "about.org" "index.org" "all-posts.org"))
             :auto-sitemap t
             :sitemap-filename "index.org"
             :sitemap-file-entry-format "%d *%t*"
             :sitemap-format-entry 'my/format-entry
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.png\"/>"
	           :html-postamble (my/html-postamble)
	           :html-link-home ti_root
	           :sitemap-function 'my/blog-sitemap-function
	           :sitemap-style 'tree
             :sitemap-sort-files 'anti-chronologically)
       (list "site-all"
             :base-directory "./blog/posts"
             :base-extension "org"
             :recursive t
	           :sitemap-title ti_title
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft" "templates" "about.org" "index.org" "all-posts.org"))
             :auto-sitemap t
             :sitemap-filename "all-posts.org"
             :sitemap-file-entry-format "%d *%t*"
             :sitemap-format-entry 'my/all-format-entry
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.png\"/>"
	           :html-postamble (my/html-postamble)
	           :html-link-home ti_root
	           :sitemap-function 'my/all-sitemap-function
	           :sitemap-style 'tree
             :sitemap-sort-files 'anti-chronologically)
       (list "site-about"
             :base-directory "./blog"
             :base-extension "org"
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft" "templates" "posts" "index.org" "about-me.org"))
             :auto-sitemap nil
             :sitemap-filename "about-me.org"
             :sitemap-file-entry-format "%d *%t*"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.png\"/>"
	           :html-postamble (my/html-postamble)
             :sitemap-style 'tree
	           :html-link-home ti_root)
       (list "site-static"
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org" "site-about" "site-static" "site-all"))))

(provide 'publish)
;;; publish.el ends here
